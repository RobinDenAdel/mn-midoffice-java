| |
:---------| -----:
![mn.png](https://bitbucket.org/repo/EgyG45X/images/3831563338-mn.png) | ![cgi.jpg](https://bitbucket.org/repo/EgyG45X/images/1202374835-cgi.jpg)

# MN Midoffice Java

In deze repository vind je de java componenten voor MN Midoffice.

## Code naar de ontwikkelomgeving halen

We maken gebruik van het versiebeheersysteem git. Eenmalig clone je de git repository naar je lokale omgeving met
```
$ git clone https://RobinDenAdel@bitbucket.org/RobinDenAdel/mn-midoffice-java.git
```

of als je je ssh keys goed hebt staan met
```
$ git clone ssh://RobinDenAdel@bitbucket.org/RobinDenAdel/mn-midoffice-java.git
```

